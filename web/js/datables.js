/* Javier Leon*/ 
$(document).ready(function() {
    /* Ejercicio 1*/
    var row = $('#2022 tr').get(3);
    row.remove();
    /* Ejercicio 2*/
    $('#crearFila').click(function () {
        var fila = $('<tr><td>Titulo</td><td>Year</td><td>Actores</td><td>Director</td><td>Duracion</td><td>Genero</td></tr>');
        var tabla2022 = $('#2022 tbody');
        tabla2022.append(fila);
    })
    /* Ejercicio 3*/
    $('#eliminarFilas').click(function (){
        $('#2022 tr').each(function(){
            let checked = $(this).find('.checkFila');
            if(checked.prop('checked')){
                $(this).remove();
            }
        })
    })
    /* Ejercicio 4*/
    var logo = $('.navbar-brand');
    logo.click(function (){
        $('.nav-link').css('background-color', 'red');
        $('.nav-link').animate({
            left: '30px',
            opacity: '0.8',
            transition: '1s',
            width: '120px'
        })
    })
})